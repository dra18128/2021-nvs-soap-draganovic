﻿using _2021_nvs_soap_draganovic.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace _2021_nvs_soap_draganovic {
    /// <summary>
    /// Summary description for SchuelerService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SchuelerService : System.Web.Services.WebService {
        public string connectionString = @"server=localhost;Database=mydb;Uid=root";
        [WebMethod]
        public Schueler GetSchueler(string sozVersNummer) {
            string query = "select * from s_schueler where s_sozVersNummer = " + sozVersNummer + ";";

            MySqlConnection conn = new MySqlConnection();
            conn.ConnectionString = connectionString;

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = query;

            conn.Open();

            MySqlDataReader reader = cmd.ExecuteReader();
            Schueler s = new Schueler();
            while (reader.Read()) {
                s.erfolg = reader.GetString("s_erfolg");
                s.gebDatum = reader.GetDateTime("s_gebDatum");
                s.name = reader.GetString("s_name");
                s.sozVersNummer = reader.GetString("s_sozVersNummer");
            }

            reader.Close();
            conn.Close();

            return s;
        }

        [WebMethod]
        public List<Schueler> getSchuelers(DateTime gebDatumVon, DateTime gebDatumBis) {
            string query = "select * from s_schueler where s_gebDatum > " + "'" + gebDatumVon.ToString("yyyy-MM-dd HH:mm:ss.fff") 
                + "'" + "& s_gebDatum < " + "'" + gebDatumBis.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'";
            MySqlConnection conn = new MySqlConnection();
            conn.ConnectionString = connectionString;

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = query;

            conn.Open();

            MySqlDataReader reader = cmd.ExecuteReader();

            List<Schueler> schuelerList = new List<Schueler>();

            while (reader.Read()) {
                schuelerList.Add(new Schueler() {
                    erfolg = reader.GetString("s_erfolg"),
                    gebDatum = reader.GetDateTime("s_gebDatum"),
                    name = reader.GetString("s_name"),
                    sozVersNummer = reader.GetString("s_sozVersNummer")
                });
            }

            reader.Close();
            conn.Close();

            return schuelerList;
        }

        [WebMethod]
        public string getSchuelerName(DateTime gebDatum) {
            string query = "select * from s_schueler where s_gebDatum = " + "'" + gebDatum.ToString("yyyy-MM-dd HH:mm:ss.fff")+ "'" + ";";

            MySqlConnection conn = new MySqlConnection();
            conn.ConnectionString = connectionString;

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = query;

            conn.Open();

            string result = "";
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read()) {
                result = reader.GetString("s_name");
            }

            reader.Close();
            conn.Close();

            return result;
        }
    }
}
